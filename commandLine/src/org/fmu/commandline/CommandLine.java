/*

 Copyright 2013, SIANI - ULPGC
 Jose Juan Hernandez Cabrera
 Jose Evora Gomez
 Johan Sebastian Cortes Montenegro

 This File is Part of JavaFMI Project

 JavaFMI Project is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 JavaFMI Project is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with JavaFMI Library.  If not, see <http://www.gnu.org/licenses/>.

 */
package org.fmu.commandline;

import org.fmu.tester.Kernel;

public class CommandLine {

    private static Kernel interpreter;

    public static void main(String[] args) {
        String path = check(args);
        interpreter = new Kernel(path);
        Console.out("Successful load");
        while (true) {
            String result = interpreter.executeCommand(Console.in());
            if (result.equals("terminated"))
                break;
            Console.out(result);
        }
    }

    private static String check(String[] args) {
        if (args.length != 1) {
            Console.out("Provide the fmu path");
            return Console.in();
        }
        return args[0];
    }
}
