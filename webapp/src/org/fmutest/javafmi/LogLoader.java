/*

 Copyright 2013, SIANI - ULPGC
 Jose Juan Hernandez Cabrera
 Jose Evora Gomez
 Johan Sebastian Cortes Montenegro

 This File is Part of JavaFMI Project

 JavaFMI Project is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 JavaFMI Project is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with JavaFMI Library.  If not, see <http://www.gnu.org/licenses/>.

 */
package org.fmutest.javafmi;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class LogLoader {

    public static List<File> load(File workingDirectory) {
        return findCsvFiles(workingDirectory);
    }

    static List<String> getLogNames(File workingDirectory) {
        ArrayList<String> lognames = new ArrayList<String>();
        for (File file : findCsvFiles(workingDirectory))
            lognames.add(file.getName().substring(0, file.getName().indexOf(".") - 1));
        return lognames;
    }

    private static List<File> findCsvFiles(File directory) {
        ArrayList<File> csvFiles = new ArrayList<File>();
        for (File fileInFolder : directory.listFiles())
            if (fileInFolder.getName().endsWith(".csv"))
                csvFiles.add(fileInFolder);
        return csvFiles;
    }
}
