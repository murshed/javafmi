/*

 Copyright 2013, SIANI - ULPGC
 Jose Juan Hernandez Cabrera
 Jose Evora Gomez
 Johan Sebastian Cortes Montenegro

 This File is Part of JavaFMI Project

 JavaFMI Project is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 JavaFMI Project is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with JavaFMI Library.  If not, see <http://www.gnu.org/licenses/>.

 */
package org.javafmi;

import java.util.ArrayList;

public class Variable<Type> {

    private final String name;
    private Type value;
    private Class classType;
    private final ArrayList<Checker> checkerList;

    public Variable(String name, Type initialValue) {
        this.name = name;
        this.value = initialValue;
        this.checkerList = new ArrayList<>();
    }

    public Class getType() {
        return value.getClass();
    }

    public Type getValue() {
        return value;
    }

    public Type getValue(TimeStamp timeStamp) {
        return null;
    }

    public String getName() {
        return name;
    }

    public void setValue(Type value) {
        if (check(value))
            this.value = value;
    }

    public void addChecker(Checker checker) {
        this.checkerList.add(checker);
    }

    private boolean check(Type value) {
        for (Checker checker : checkerList)
            if (!checker.check(value))
                return false;
        return true;
    }
}
