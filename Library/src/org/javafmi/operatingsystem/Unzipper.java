/*

 Copyright 2013, SIANI - ULPGC
 Jose Juan Hernandez Cabrera
 Jose Evora Gomez
 Johan Sebastian Cortes Montenegro

 This File is Part of JavaFMI Project

 JavaFMI Project is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 JavaFMI Project is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with JavaFMI Library.  If not, see <http://www.gnu.org/licenses/>.

 */
package org.javafmi.operatingsystem;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

public class Unzipper {

    private File fmuFile;

    public Unzipper(File fmuFile) {
        if (checkInputFile(fmuFile))
            this.fmuFile = fmuFile;
    }

    private boolean checkInputFile(File fmuFile) {
        return (fmuFile.exists());
    }

    public File unzip(String fileName, String outPath) {
        return doUnzip(fileName, createOutputFolder(outPath));
    }

    private File createOutputFolder(String outputFolder) {
        File outFile = new File(outputFolder);
        if (!outFile.exists()) outFile.mkdirs();
        return outFile;
    }

    private File doUnzip(String fileName, File outPath) {
        ZipInputStream fmuZipInputStream = new ZipInputStream(getInputStreamForFmufile());
        return unzipEntryIn(fmuZipInputStream, searchEntry(fmuZipInputStream, fileName), outPath);
    }

    private File unzipEntryIn(ZipInputStream zipInputStream, ZipEntry zipEntry, File outPath) {
        File destinationFile = getFileDestinationFile(outPath, zipEntry);
        copyTo(zipInputStream, getOutputStreamForFile(destinationFile));
        return destinationFile;
    }

    private FileInputStream getInputStreamForFmufile() {
        try {
            return new FileInputStream(fmuFile);
        } catch (FileNotFoundException ex) {
            Logger.getLogger(Unzipper.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    private ZipEntry searchEntry(ZipInputStream zipInputStream, String pathInsideZip) {
        try {
            ZipEntry zipEntry;
            while ((zipEntry = zipInputStream.getNextEntry()) != null)
                if (isFileDesired(zipEntry, pathInsideZip))
                    return zipEntry;
        } catch (IOException ex) {
            Logger.getLogger(Unzipper.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    private boolean isFileDesired(ZipEntry zipEntry, String fileName) {
        return zipEntry.getName().replace("/", File.separator).equals(fileName.replace("/", File.separator));
    }

    private void copyTo(ZipInputStream zipInputStream, BufferedOutputStream bufferedOutputStream) {
        int currentBytesRead;
        byte[] buffer = new byte[4096];
        try {
            while (true) {
                currentBytesRead = zipInputStream.read(buffer, 0, buffer.length);
                if (currentBytesRead < 0) break;
                bufferedOutputStream.write(buffer, 0, currentBytesRead);
            }
            bufferedOutputStream.flush();
            bufferedOutputStream.close();
            zipInputStream.close();
        } catch (IOException ex) {
            Logger.getLogger(Unzipper.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private BufferedOutputStream getOutputStreamForFile(File copiedFile) {
        try {
            return new BufferedOutputStream(new FileOutputStream(copiedFile));
        } catch (FileNotFoundException ex) {
            Logger.getLogger(Unzipper.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    private File getFileDestinationFile(File outPath, ZipEntry zipEntry) {
        try {
            File copiedFile = new File(getFilenameWithoutDirectories(outPath, zipEntry));
            copiedFile.createNewFile();
            return copiedFile;
        } catch (IOException ex) {
            Logger.getLogger(Unzipper.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    private String getFilenameWithoutDirectories(File outPath, ZipEntry zipEntry) {
        String fileNameWithNoDirectories = zipEntry.getName();
        fileNameWithNoDirectories = fileNameWithNoDirectories.substring(fileNameWithNoDirectories.lastIndexOf("/") + 1);
        return PathBuilder.buildPath(outPath.getPath(), fileNameWithNoDirectories);
    }
}
