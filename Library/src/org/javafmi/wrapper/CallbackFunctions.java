/*

 Copyright 2013, SIANI - ULPGC
 Jose Juan Hernandez Cabrera
 Jose Evora Gomez
 Johan Sebastian Cortes Montenegro

 This File is Part of JavaFMI Project

 JavaFMI Project is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 JavaFMI Project is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with JavaFMI Library.  If not, see <http://www.gnu.org/licenses/>.

 */
package org.javafmi.wrapper;

import com.sun.jna.Structure;
import java.util.Arrays;
import java.util.List;
import org.javafmi.wrapper.FunctionalMockupInterface.CallbackAllocateMemory;
import org.javafmi.wrapper.FunctionalMockupInterface.CallbackFreeMemory;
import org.javafmi.wrapper.FunctionalMockupInterface.CallbackLogger;
import org.javafmi.wrapper.FunctionalMockupInterface.CallbackStepFinished;

public class CallbackFunctions extends Structure {

    public CallbackLogger logger;
    public CallbackAllocateMemory allocateMemory;
    public CallbackFreeMemory freeMemory;
    public CallbackStepFinished stepFinished;

    public CallbackFunctions() {
        super();
        this.logger = new FunctionalMockupUnit.Logger();
        this.allocateMemory = new FunctionalMockupUnit.AllocateMemory();
        this.freeMemory = new FunctionalMockupUnit.FreeMemory();
        this.stepFinished = new FunctionalMockupUnit.StepFinished();
        setAlignType(ALIGN_GNUC);
    }

    @Override
    protected List getFieldOrder() {
        return Arrays.asList(new String[]{"logger", "allocateMemory", "freeMemory", "stepFinished"});
    }

    public static class ByValue extends CallbackFunctions implements Structure.ByValue {

        public ByValue() {
            super();
        }
    }
}
