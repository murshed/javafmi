/*

 Copyright 2013, SIANI - ULPGC
 Jose Juan Hernandez Cabrera
 Jose Evora Gomez
 Johan Sebastian Cortes Montenegro

 This File is Part of JavaFMI Project

 JavaFMI Project is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 JavaFMI Project is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with JavaFMI Library.  If not, see <http://www.gnu.org/licenses/>.

 */
package org.javafmi;

import java.util.LinkedHashMap;
import java.util.Map;

public class Variant<Type> {

    private Variable<Type> variable;
    private Map<TimeStamp, Type> valuesLog;

    public Variant(Variable<Type> variable) {
        this.variable = variable;
        this.valuesLog = new LinkedHashMap<>();
    }

    public void Log(TimeStamp timeStamp, Type value) {
        valuesLog.put(timeStamp, value);
    }

    public Variable<Type> getVariable() {
        return variable;
    }

    public Map<TimeStamp, Type> getValuesLog() {
        return valuesLog;
    }
}
