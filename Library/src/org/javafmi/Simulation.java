/*

 Copyright 2013, SIANI - ULPGC
 Jose Juan Hernandez Cabrera
 Jose Evora Gomez
 Johan Sebastian Cortes Montenegro

 This File is Part of JavaFMI Project

 JavaFMI Project is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 JavaFMI Project is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with JavaFMI Library.  If not, see <http://www.gnu.org/licenses/>.

 */
package org.javafmi;

import java.util.ArrayList;
import java.util.List;
import org.javafmi.modeldescription.modelvariables.ScalarVariable;
import org.javafmi.wrapper.FmuProxy;

public class Simulation {

    private static final int DEFAULT_TIMEOUT = 1000;
    private FmuLocation fmuLocation;
    private FmuProxy fmuProxy;
    private String proxyName;

    public Simulation(String fmuPath) {
        this(fmuPath, DEFAULT_TIMEOUT);
    }

    public Simulation(String fmuPath, double timeout) {
        this.fmuLocation = new FmuLocation(fmuPath);
        this.fmuProxy = new FmuProxy(fmuLocation);
        this.proxyName = fmuProxy.getModelDescription().getModelName();
        fmuProxy.instantiateSlave(timeout);
    }

    public Status init(double startTime) {
        return init(startTime, Double.MAX_VALUE);
    }

    public Status init(double startTime, double stopTime) {
        return fmuProxy.initializeSlave(startTime, stopTime);
    }

    public Status doStep(double stepSize) {
        return fmuProxy.doStep(stepSize);
    }

    public Status cancelStep() {
        return fmuProxy.cancelStep();
    }

    public Status terminate() {
        Status terminateStatus = fmuProxy.terminateSlave();
        fmuProxy.freeSlave();
        return terminateStatus;
    }

    public Status resetSlave() {
        return fmuProxy.resetSlave();
    }

    public Variable readVariable(String variableName) {
        return VariableReader.getInstance().read(variableName, fmuProxy);
    }

    public Status writeVariable(String name, Object value) {
        return VariableWriter.getInstance().write(name, value, fmuProxy);
    }

    public List<String> listVariables() {
        ModelDescription modelDescription = fmuProxy.getModelDescription();
        ArrayList<String> variables = new ArrayList<>();
        for (ScalarVariable scalarVariable : modelDescription.getModelVariables())
            variables.add(scalarVariable.getName());
        return variables;
    }

    public boolean isInstanceVisible() {
        return fmuProxy.isInstanceVisible();
    }

    public boolean isInstanceInteractive() {
        return fmuProxy.isInstanceInteractive();
    }

    public boolean isDoingLogging() {
        return fmuProxy.isDoingLogging();
    }

    public String getInternalName() {
        return proxyName;
    }
}
