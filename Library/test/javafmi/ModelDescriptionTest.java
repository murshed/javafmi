/*

 Copyright 2013, SIANI - ULPGC
 Jose Juan Hernandez Cabrera
 Jose Evora Gomez
 Johan Sebastian Cortes Montenegro

 This File is Part of JavaFMI Project

 JavaFMI Project is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 JavaFMI Project is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with JavaFMI Library.  If not, see <http://www.gnu.org/licenses/>.

 */
package javafmi;

import java.io.File;
import java.util.ArrayList;
import org.javafmi.ModelDescription;
import org.junit.Test;
import org.simpleframework.xml.Serializer;
import org.simpleframework.xml.core.Persister;

public class ModelDescriptionTest {

    private static String modelDescriptionResourcesPath = "resources/modelDescription/";
    private static String modelDescriptionFilename = "modelDescription.xml";

    @Test
    public void testCheckAllModelDescriptionExamples() throws Exception {
        for (File file : getModelDescriptionFiles(new File(modelDescriptionResourcesPath))) {
            Serializer serializer = new Persister();
            serializer.read(ModelDescription.class, file);
        }
    }

    private ArrayList<File> getModelDescriptionFiles(File file) {
        ArrayList<File> fileList = new ArrayList<>();
        if (file.isDirectory())
            for (File child : file.listFiles())
                fileList.addAll(getModelDescriptionFiles(child));
        else if (file.getName().equals(modelDescriptionFilename))
            fileList.add(file);
        return fileList;
    }
}
