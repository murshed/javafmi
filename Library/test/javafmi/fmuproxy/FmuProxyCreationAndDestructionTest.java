/*

 Copyright 2013, SIANI - ULPGC
 Jose Juan Hernandez Cabrera
 Jose Evora Gomez
 Johan Sebastian Cortes Montenegro

 This File is Part of JavaFMI Project

 JavaFMI Project is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 JavaFMI Project is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with JavaFMI Library.  If not, see <http://www.gnu.org/licenses/>.

 */
package javafmi.fmuproxy;

import static javafmi.Common.*;
import org.javafmi.Status;
import org.javafmi.wrapper.FmuProxy;
import org.junit.Assert;
import org.junit.Test;

public class FmuProxyCreationAndDestructionTest {

    @Test
    public void testInstanceCreationAndDestruction() {
        FmuProxy proxy = getProxyForBouncingBall();
        instantiateProxy(proxy);
        Assert.assertEquals(Status.OK, initializeProxy(proxy));
        Assert.assertEquals(Status.OK, proxy.terminateSlave());
        proxy.freeSlave();
    }

    @Test
    public void testSetDebugSlave() {
        FmuProxy proxy = getProxyForBouncingBall();
        instantiateProxy(proxy);
        initializeProxy(proxy);
        Assert.assertFalse(proxy.isDoingLogging());
        Status status = proxy.setIsDoingLogging(true);
        Assert.assertEquals(Status.OK, status);
        Assert.assertTrue(proxy.isDoingLogging());
        proxy.terminateSlave();
        proxy.freeSlave();
    }

    @Test
    public void testDoStepAndReset() {
        FmuProxy proxy = getProxyForBouncingBall();
        instantiateProxy(proxy);
        double startTime = 0;
        double stopTime = 3;
        double stepSize = 1;
        Assert.assertEquals(Status.OK, proxy.initializeSlave(startTime, stopTime));
        Assert.assertEquals(Status.OK, proxy.doStep(stepSize));
        Assert.assertEquals(Status.OK, proxy.resetSlave());
        Assert.assertEquals(Status.OK, proxy.initializeSlave(startTime, stopTime));
        proxy.terminateSlave();
        proxy.freeSlave();
    }
}
