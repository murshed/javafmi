/*

 Copyright 2013, SIANI - ULPGC
 Jose Juan Hernandez Cabrera
 Jose Evora Gomez
 Johan Sebastian Cortes Montenegro

 This File is Part of JavaFMI Project

 JavaFMI Project is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 JavaFMI Project is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with JavaFMI Library.  If not, see <http://www.gnu.org/licenses/>.

 */
package javafmi.operatingsystem;

import junit.framework.Assert;
import org.javafmi.operatingsystem.PathBuilder;
import org.junit.Test;

public class PathBuilderTest {

    @Test
    public void testBuildPathOneFolderOneFIle() {
        if (systemIsWindows())
            Assert.assertEquals("folder\\file", PathBuilder.buildPath("folder", "file"));
        else if (systemIsMac())
            Assert.assertEquals("folder/file", PathBuilder.buildPath("folder", "file"));
    }

    @Test
    public void testBuildPathWithFolders() {
        String[] args = {"this", "is", "a", "path"};
        if (systemIsWindows())
            Assert.assertEquals("this\\is\\a\\path\\", PathBuilder.buildPath(args));
        else if (systemIsMac())
            Assert.assertEquals("this/is/a/path/", PathBuilder.buildPath(args));
    }

    @Test
    public void testBuildPathWithFoldersAndFile() {
        String[] args = {"this", "is", "a", "path"};
        if (systemIsWindows())
            Assert.assertEquals("this\\is\\a\\path\\filename", PathBuilder.buildPath(args, "filename"));
        else if (systemIsMac())
            Assert.assertEquals("this/is/a/path/filename", PathBuilder.buildPath(args, "filename"));

    }

    private boolean systemIsWindows() {
        return System.getProperty("os.name").toLowerCase().startsWith("win");
    }

    private boolean systemIsMac() {
        return System.getProperty("os.name").toLowerCase().startsWith("mac");
    }
}
