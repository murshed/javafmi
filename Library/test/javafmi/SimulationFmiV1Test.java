/*

 Copyright 2013, SIANI - ULPGC
 Jose Juan Hernandez Cabrera
 Jose Evora Gomez
 Johan Sebastian Cortes Montenegro

 This File is Part of JavaFMI Project

 JavaFMI Project is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 JavaFMI Project is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with JavaFMI Library.  If not, see <http://www.gnu.org/licenses/>.

 */
package javafmi;

import static javafmi.Common.*;
import static junit.framework.Assert.*;
import org.javafmi.Simulation;
import org.javafmi.Status;
import org.junit.Test;

public class SimulationFmiV1Test {

    private final double TIME_STEP = 1;
    private final double START_TIME = 1;
    private final double STOP_TIME = 50;
    private final double TIMEOUT = 10000;

    @Test
    public void usageOfFrequencyModelFMUv1NoTimeout() {
        Simulation simulation = new Simulation(getFmuPath("SFRV1CS.fmu"));
        executeCallSequenceOfCosimulation(simulation);
    }

    @Test
    public void usageOfFrequencyModelFMUv1WithTimeout() {
        Simulation simulation = new Simulation(getFmuPath("SFRV1CS.fmu"), TIMEOUT);
        executeCallSequenceOfCosimulation(simulation);
    }

    @Test
    public void simulationWhenLibraryHasModelIdentifierAsName() {
        Simulation simulation = new Simulation(getFmuPath("GridSysPro_0V1_Examples_Validation_0with_0generator.fmu"));
        assertNotNull(simulation);
        simulation.terminate();
    }

    @Test
    public void readingAEnumeration() {
        Simulation simulation = new Simulation(getFmuPath("GridSysPro_0V1_Examples_Validation_0with_0generator.fmu"));
        simulation.init(0);
        assertEquals("InitialOutput", simulation.readVariable("regulation_vitesse.limIntegrator.initType").getValue());
        assertEquals("InitialOutput", simulation.readVariable("regulation_vitesse.transferFunction.initType").getValue());
        simulation.terminate();
    }

    @Test
    public void readingAEnumerationWhithoutDoingInit() {
        Simulation simulation = new Simulation(getFmuPath("GridSysPro_0V1_Examples_Validation_0with_0generator.fmu"));
        assertEquals("InitialOutput", simulation.readVariable("regulation_vitesse.limIntegrator.initType").getValue());
        assertEquals("InitialOutput", simulation.readVariable("regulation_vitesse.transferFunction.initType").getValue());
        simulation.terminate();
    }

    private String getFmuPath(String fmuName) {
        return getFmuLocationFromName(fmuName).toString();
    }

    private void executeCallSequenceOfCosimulation(Simulation simulation) {
        assertEquals(Status.OK, simulation.init(START_TIME, STOP_TIME));
        assertEquals(Status.OK, simulation.doStep(TIME_STEP));
        assertEquals(Status.OK, simulation.writeVariable("Pd", 0.3));
        assertEquals(Status.OK, simulation.doStep(TIME_STEP));
        assertEquals(0.3, simulation.readVariable("Pd").getValue());
        for (int i = 0; i < 47; i++)
            assertEquals(Status.OK, simulation.doStep(TIME_STEP));
        for (int i = 0; i < 10; i++)
            assertEquals(Status.ERROR, simulation.doStep(TIME_STEP));
        assertEquals(Status.OK, simulation.terminate());
    }
}
