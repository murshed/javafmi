/*

 Copyright 2013, SIANI - ULPGC
 Jose Juan Hernandez Cabrera
 Jose Evora Gomez
 Johan Sebastian Cortes Montenegro

 This File is Part of JavaFMI Project

 JavaFMI Project is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 JavaFMI Project is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with JavaFMI Library.  If not, see <http://www.gnu.org/licenses/>.

 */
package org.fmu.tester.command;

import org.fmu.tester.Command;
import org.fmu.tester.Simulation;
import org.fmu.tester.VariableLogger;
import org.fmu.tester.VariableLoggerList;

public class CommandMultiStep extends Command {

    private final double stepSize;
    private final int numberOfSteps;

    public CommandMultiStep(String stepSize, String numberOfSteps, VariableLoggerList variableLoggerList) {
        super(variableLoggerList);
        this.stepSize = Double.parseDouble(stepSize);
        this.numberOfSteps = Integer.parseInt(numberOfSteps);
    }

    @Override
    public String execute(Simulation simulation) {
        try {
            for (int i = 0; i < numberOfSteps; i++) {
                simulation.doStep(stepSize);
                log(simulation);
            }
            return "Simulation stepped";
        } catch (Exception e) {
            return "Error: doing step";
        }
    }

    private void log(Simulation simulation) {
        for (VariableLogger logger : this.getVariableLoggerList())
            logger.log(simulation.getTime(), "" + simulation.readVariable(logger.getVariableName()).getValue());
    }

    public static String getUsage() {
        return "multistep <step size> <number of steps>";
    }
}