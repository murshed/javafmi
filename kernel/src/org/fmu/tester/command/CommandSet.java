/*

 Copyright 2013, SIANI - ULPGC
 Jose Juan Hernandez Cabrera
 Jose Evora Gomez
 Johan Sebastian Cortes Montenegro

 This File is Part of JavaFMI Project

 JavaFMI Project is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 JavaFMI Project is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with JavaFMI Library.  If not, see <http://www.gnu.org/licenses/>.

 */
package org.fmu.tester.command;

import org.fmu.tester.Command;
import org.fmu.tester.Simulation;
import org.fmu.tester.VariableLoggerList;
import org.javafmi.Variable;
import org.javafmi.modeldescription.typedefinitions.BooleanType;
import org.javafmi.modeldescription.typedefinitions.IntegerType;
import org.javafmi.modeldescription.typedefinitions.RealType;
import org.javafmi.modeldescription.typedefinitions.StringType;

public class CommandSet extends Command {

    private final String variableName;
    private final String value;

    public CommandSet(String variableName, String value, VariableLoggerList variableLoggerList) {
        super(variableLoggerList);
        this.variableName = variableName;
        this.value = value;
    }

    @Override
    public String execute(Simulation simulation) {
        try {
            Variable<?> variable = simulation.readVariable(variableName);
            if (variable.getType().equals(RealType.class))
                simulation.writeVariable(variableName, Double.parseDouble(value));
            if (variable.getType().equals(IntegerType.class))
                simulation.writeVariable(variableName, Integer.parseInt(value));
            if (variable.getType().equals(BooleanType.class))
                simulation.writeVariable(variableName, Boolean.parseBoolean(value));
            if (variable.getType().equals(StringType.class))
                simulation.writeVariable(variableName, value);
            return "Variable set";
        } catch (Exception e) {
            return "Error: Impossible to set the variable. Variable or value are well writen?";
        }
    }

    public static String getUsage() {
        return "set <varname> <value>";
    }
}
